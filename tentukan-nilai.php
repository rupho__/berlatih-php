<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>

<?php
    function tentukan_nilai($number)
    {
        //  kode disini
        if($number >= 85 && $number <= 100){
            return "Sangat Baik <br>";
        } elseif ($number >= 70 && $number < 85) {
             return "Baik  <br>";
        } elseif ($number >= 60 && $number < 70) {
             return "Cukup  <br>";
        } elseif ($number >=0 && $number <60) {
            return "Kurang  <br>";
        } else {
            return "Nilai Tidak Terpenuhi";
        }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    // echo tentukan_nilai(-1000);
?>
    
</body>
</html>